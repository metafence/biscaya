#!/usr/bin/env bash


# make sure script exits with first error
set -e

assert_available hcloud
assert_available gawk
assert_available openssl
assert_available curl
assert_exported SYNCTHING_USER
assert_exported SYNCTHING_PASS

ALL_NODE_NAMES=$(hcloud server list --selector cluster=${CLUSTER_NAME} | grep ${CLUSTER_NAME} | gawk '{print $2}' | sort)

echo ALL_NODE_NAMES=$ALL_NODE_NAMES

export SYNCTHING_API_KEY=${SYNCTHING_API_KEY:-$(openssl rand -base64 32)}
echo setting syncthing api key to $SYNCTHING_API_KEY 

echo Collecting IPs

# Determine node names and IPs dynamically
declare -A NODE_IPS
for ((i=0; i<NODE_COUNT; i++)); do
    node_name="${CLUSTER_NAME}-node-$i"
    NODE_IPS[$node_name]=$(hcloud server ip $node_name)
done

echo install and startup syncthing on all nodes of cloud
for node_name in ${ALL_NODE_NAMES}; do
    ip=${NODE_IPS["$node_name"]}
    echo ----------------------------------------------------------------------
    echo "clici: setting up syncthing on $node_name ($ip) ..."
    echo ----------------------------------------------------------------------
    envsubst < $PHASE_DIR/install-syncthing.sh.template > $ARTIFACTS_ROOT/install-syncthing.sh.$node_name   
    $SCP $ARTIFACTS_ROOT/install-syncthing.sh.$node_name root@$ip:/tmp/install-syncthing.sh
    $SSH root@$ip chmod +x /tmp/install-syncthing.sh
    $SSH root@$ip /tmp/install-syncthing.sh
    envsubst < $PHASE_DIR/syncthing.config.xml.template > $ARTIFACTS_ROOT/syncthing.config.xml.$node_name
    $SCP $ARTIFACTS_ROOT/syncthing.config.xml.$node_name root@$ip:/srv/syncthing/.local/state/syncthing/config.xml
    $SSH root@$ip chown syncthing:syncthing /srv/syncthing/.local/state/syncthing/config.xml
    $SSH root@$ip systemctl start syncthing@syncthing.service
    echo sleep some
    sleep 5


done

# create user and password via API
echo sleep some
sleep 10

for node_name in ${ALL_NODE_NAMES}; do
    ip=$(hcloud server ip $node_name)

    json_data=$(cat <<EOF
    {
    "gui": {
        "enabled": true,
        "address": "0.0.0.0:8384",
        "user": "${SYNCTHING_USER}",
        "password": "${SYNCTHING_PASS}"
    }
    }
EOF
)

    curl -k -X POST -H "X-API-Key: ${SYNCTHING_API_KEY}" -H "Content-Type: application/json" \
        -d "$json_data" "https://$ip:8384/rest/system/config"


    echo reach syncthing at $node_name with https://$ip:8384 and user $SYNCTHING_USER
done


# to get rid of lingering exit codes from the commands above, at the end of script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase
exit 0
