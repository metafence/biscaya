#!/usr/bin/env bash

# connect to cluster node 0 
# if given, connect to node index in parameter

# make sure script exits with first error
set -e

node_index=${PHASE_PARAMS[0]:-0}
server_name=${CLUSTER_NAME}-node-${node_index}
ip=$(hcloud server ip $server_name)
SSH_CMD="$SSH root@$ip"

echo "Connecting to node $node_index ($server_name) with $SSH..."
$SSH_CMD

# to get rid of lingering exit codes from the commands above, at the end of script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase
exit 0
