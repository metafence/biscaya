#!/usr/bin/env bash

# A script to bootstrap a local dev environment


# make sure script exits with first error
set -e

# fully update system
sudo apt update
sudo apt upgrade -y

# install tooling
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg

# install opentofu

# setup open tofu repo
if [ ! -f /etc/apt/sources.list.d/opentofu.list ]; then
    echo 'Setze opentofu repo auf' 
    sudo install -m 0755 -d /etc/apt/keyrings
    curl -fsSL https://get.opentofu.org/opentofu.gpg | sudo tee /etc/apt/keyrings/opentofu.gpg >/dev/null
    curl -fsSL https://packages.opentofu.org/opentofu/tofu/gpgkey | sudo gpg --no-tty --batch --dearmor -o /etc/apt/keyrings/opentofu-repo.gpg >/dev/null
    sudo chmod a+r /etc/apt/keyrings/opentofu.gpg /etc/apt/keyrings/opentofu-repo.gpg
    echo \
    "deb [signed-by=/etc/apt/keyrings/opentofu.gpg,/etc/apt/keyrings/opentofu-repo.gpg] https://packages.opentofu.org/opentofu/tofu/any/ any main
    deb-src [signed-by=/etc/apt/keyrings/opentofu.gpg,/etc/apt/keyrings/opentofu-repo.gpg] https://packages.opentofu.org/opentofu/tofu/any/ any main" | \
    sudo tee /etc/apt/sources.list.d/opentofu.list > /dev/null
    sudo chmod a+r /etc/apt/sources.list.d/opentofu.list
    sudo apt-get update
fi

if [ ! $(which tofu) ]; then
    echo installiere tofu
    sudo apt-get install -y tofu
fi

# Install hcloud
if command -v hcloud >/dev/null 2>&1; then
    echo "hcloud ist bereits installiert."
    hcloud version
else

    echo "Installiere hcloud CLI..."

    # Lade die neueste Release-Information von GitHub
    HCLOUD_LATEST=$(curl -s https://api.github.com/repos/hetznercloud/cli/releases/latest | jq -r '.tag_name')

    # Setze die URL für den Download
    HCLOUD_URL="https://github.com/hetznercloud/cli/releases/download/${HCLOUD_LATEST}/hcloud-linux-amd64.tar.gz"

    # Lade das Archiv herunter und entpacke es
    cd $ARTIFACTS_ROOT
    curl -L $HCLOUD_URL | tar zx

    # Verschiebe die ausführbare Datei in einen binären Pfad
    sudo mv hcloud /usr/local/bin/hcloud

    # Mache die Datei ausführbar
    sudo chmod +x /usr/local/bin/hcloud

    echo "Installation abgeschlossen."
    hcloud version
fi

# Install consul
if command -v consul >/dev/null 2>&1; then
    echo "consul ist bereits installiert."
    hcloud version
else
    cd $ARTIFACTS_ROOT
    echo Lade Consul herunter
    curl -o consul.zip "https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip"

    echo Entpacke das Archiv und verschiebe die Binärdatei
    unzip consul.zip
    sudo mv consul /usr/local/bin/
    sudo chmod +x /usr/local/bin/consul
    consul version
    
fi

# cleanup
sudo apt autoclean



# to get rid of lingering exit codes from the commands above, at the end of script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase
exit 0
