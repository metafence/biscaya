#!/usr/bin/env bash

# destroy cluster

# make sure script exits with first error
set -e

# check env
assert_exported HCLOUD_API_TOKEN
assert_exported CLUSTER_NAME
assert_available tofu
assert_available hcloud

cd $ARTIFACTS_ROOT

export HCLOUD_TOKEN=$HCLOUD_API_TOKEN
terraform destroy
unset HCLOUD_TOKEN 

hcloud context delete ${CLUSTER_NAME}

# to get rid of lingering exit codes from the commands above, at the end of script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase
exit 0
