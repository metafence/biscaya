#!/usr/bin/env bash

# ramp up the initial consul cluster with 3 nodes

set -e

# check  environment
assert_exported HCLOUD_API_TOKEN
assert_exported CLUSTER_NAME
assert_exported CONSUL_VERSION

# check needed tools
assert_available envsubst
assert_available tofu
assert_available hcloud
assert_available consul

echo configure hcloud context...

existing_context=$(hcloud context list | grep "${CLUSTER_NAME}" || true)
echo existing_context="'$existing_context'"

if [ "$existing_context" == "" ]; then
    export HCLOUD_TOKEN=$HCLOUD_API_TOKEN
    echo "Y" | hcloud context create ${CLUSTER_NAME}
    unset HCLOUD_TOKEN 
else
    echo "context ${CLUSTER_NAME} already exists"
fi

hcloud context use ${CLUSTER_NAME}

echo Check if any servers already exist with the name prefix
existing_servers=$(hcloud server list | grep "${CLUSTER_NAME}-node" || true)
echo existing_servers=$existing_servers
if [ ! -z "$existing_servers" ]; then
  echo "A cluster with the name ${CLUSTER_NAME} already exists. Aborting bootstrapping script."
  exit 1
fi

# Generate the Terraform configuration file from the template
envsubst < ${PHASE_DIR}/bootstrap.tf.template > ${ARTIFACTS_ROOT}/main.tf

# Initialize and apply Terraform
cd ${ARTIFACTS_ROOT}

# Determine node names and IPs dynamically
declare -A NODE_IPS
RETRY_JOIN_IPS=""
for ((i=0; i<NODE_COUNT; i++)); do
    node_name="${CLUSTER_NAME}-node-$i"
    ip=$(hcloud server ip $node_name)
    NODE_IPS[$node_name]=$ip
    if [[ -n $RETRY_JOIN_IPS ]]; then
        RETRY_JOIN_IPS+=","
    fi
    RETRY_JOIN_IPS+="\"$ip\""
done

export RETRY_JOIN_IPS

# Retrieve hetzner data center that's used
export DATA_CENTER=$(hcloud server describe ${CLUSTER_NAME}-node-0 --output json | jq -r .datacenter.name)

# Install consul
cd ${STARTUP_DIR}
export ENCRYPTION_KEY=$(consul keygen)
echo "Consul encryption key is $ENCRYPTION_KEY"
envsubst < ${PHASE_DIR}/install-consul.sh.template > ${ARTIFACTS_ROOT}/install-consul-${CONSUL_VERSION}.sh

for node_name in "${!NODE_IPS[@]}"; do
    ip=${NODE_IPS[$node_name]}
 
    echo ----------------------------------------------------------------------
    echo "clici: Setting up consul on node $node_name ($ip) ..."  
    echo ----------------------------------------------------------------------

    # Warte, bis cloud-init abgeschlossen ist
    echo "Waiting for cloud-init to complete on $node_name..."
    while ! $SSH root@$ip cloud-init status | grep -qw "done"; do
        echo "cloud-init is still running on $node_name, waiting 5 seconds..."
        sleep 5
    done
    echo "cloud-init completed on $node_name, proceeding with script deployment..."

    while true; do
        echo "Copying script install-consul-${CONSUL_VERSION}.sh to $node_name ($ip) ..."    
        if $SCP ${ARTIFACTS_ROOT}/install-consul-${CONSUL_VERSION}.sh root@$ip:/tmp; then
            echo 'Executing script ...'
            if $SSH root@$ip bash /tmp/install-consul-${CONSUL_VERSION}.sh; then
                break  # Exit the loop if the script was successful
            fi
        fi
        echo "Sleep some..."
        sleep 5
    done
done

# Output the URLs for the Consul UI
for node_name in "${!NODE_IPS[@]}"; do
    ip=${NODE_IPS[$node_name]}
    echo "You should now be able to see the Consul UI on http://$ip:8500/ui"
done


exit 0
