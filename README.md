# biscaya

a digital home in the internet, completely managed by you:

- consul cluster
- syncthing cluster
- ssh terminal

## planned

- ssh also on port 80 and 443
- ssh back channel
- tor gateway


# setup some internet facing cluster for rough times

# clici phases

This project uses [metafence.gitlab.io/clici](https://metafence.gitlab.io/clici/) to manage separate tasks. 

## How to setup your local dev env

- install clici from the local copy of this repo

```bash
sudo scripts/clici install
```

- bootstrap local dev environment
```bash
clici bootstrap-local
```

## Bootstrap consul cluster on hetzner cloud.

```bash
clici bootstrap-consul
```

Make sure to have __HCLOUD_API_TOKEN__ in environments/local/secrets.source exported to a valid hetzner cloud api token, as well as __CLUSTER_NAME__ in environments/local/env.source exported something meaningful for you.

## Bootstrap syncthing on all nodes.

```bash
clici bootstrap-syncthing
```

Make sure to run everything in correct sequence

# Configuring an environment

To configure a specific environment (as an example myEnv, apart from local), you have to set the following keys:

| File                             | Key              | Default   | Description                                                          |
| -------------------------------- | ---------------- | --------- | -------------------------------------------------------------------- |
| environments/myEnv/env.source    | CONSUL_VERSION   | 1.18.1    | Version of consul you want to use |
| -same-                           | CLUSTER_NAME     | ephemeral | Name of the consul cluster                                           | 
| -same-                           | NODE_COUNT       | 3         | amount of cluster nodes                                              | 
| environments/myEnv/secret.source | HCLOUD_API_TOKEN |           | a valid hetzner cloud project specific API token                     |
| -same-                           | SYNCTHING_USER   |           | a user to be set as syncthing ui user                                |
| -same-                           | SYNCTHING_PASS   |           | cleartext password of that user; will get hashed by the script       |
| -same-                           | SYNCTHING_API_KEY| random    | an api key to set.                                                   |


find defaults, if any, and more elaborate explanations, in phases/env.source


make sure to export all values, like
```bash
export SYNCTHING_USER="micwin"
```

then, somewhere in your shell, set

```bash
export ENVIRONMENT=myEnv
```
# Caveats

- donnot clici clean before clici destroy - otherwise you will lose your terraform state and will have to delete  instances and ssh keys manually
- in order to avoid accidential shutdown of an existing cluster, clici bootstrap-consul checks for already existing installments. So in order to replace a cluster by a new installment, 
  you have to explicitely clici destroy the existing cluster first
 

